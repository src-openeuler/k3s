#!/bin/bash

set -e
if [ ! -f ./v1.24.2+rc1+k3s2.tar.gz ];then
  echo "this script should be executed in source directory"
  exit
fi

tar -xvf v1.24.2+rc1+k3s2.tar.gz

TRAEFIK_CHART_VERSION="10.19.3"
TRAEFIK_PACKAGE_VERSION="00"
TRAEFIK_FILE=traefik-${TRAEFIK_CHART_VERSION}${TRAEFIK_PACKAGE_VERSION}.tgz
TRAEFIK_CRD_FILE=traefik-crd-${TRAEFIK_CHART_VERSION}${TRAEFIK_PACKAGE_VERSION}.tgz
TRAEFIK_URL=https://helm.traefik.io/traefik/traefik-${TRAEFIK_CHART_VERSION}.tgz
CHARTS_DIR=build/static/charts
RUNC_DIR=build/src/github.com/opencontainers/runc
CONTAINERD_DIR=build/src/github.com/containerd/containerd
DATA_DIR=build/data
TMP_DIR=.
export TZ=UTC

umask 022
rm -rf ${CHARTS_DIR}
rm -rf ${RUNC_DIR}
rm -rf ${CONTAINERD_DIR}
mkdir -p ${CHARTS_DIR}
mkdir -p ${DATA_DIR}


download_and_package_traefik () {
  echo "Downloading Traefik Helm chart from ${TRAEFIK_URL}"
  curl -k -sfL ${TRAEFIK_URL} -o ${TMP_DIR}/${TRAEFIK_FILE}
  code=$?

  if [ $code -ne 0 ]; then
    echo "Error: Failed to download Traefik Helm chart!"
    exit $code
  fi

  echo "Uncompress ${TMP_DIR}/${TRAEFIK_FILE}"
  tar xf ${TMP_DIR}/${TRAEFIK_FILE} -C ${TMP_DIR}

  echo "Prepare traefik CRD"
  TRAEFIK_TMP_CHART=${TMP_DIR}/traefik
  TRAEFIK_TMP_CRD=${TRAEFIK_TMP_CHART}-crd

  # Collect information on chart
  name="traefik"
  api_version="v2"
  chart_version=${TRAEFIK_CHART_VERSION}
  package_version=${TRAEFIK_PACKAGE_VERSION}

  # Collect information on CRDs
  crd_apis=()

  crd_apis+=("traefik.containo.us/v1alpha1/IngressRouteTCP")
  crd_apis+=("traefik.containo.us/v1alpha1/IngressRouteUDP")
  crd_apis+=("traefik.containo.us/v1alpha1/IngressRoute")
  crd_apis+=("traefik.containo.us/v1alpha1/MiddlewareTCP")
  crd_apis+=("traefik.containo.us/v1alpha1/Middleware")
  crd_apis+=("traefik.containo.us/v1alpha1/ServersTransport")
  crd_apis+=("traefik.containo.us/v1alpha1/TLSOption")
  crd_apis+=("traefik.containo.us/v1alpha1/TLSStore")
  crd_apis+=("traefik.containo.us/v1alpha1/TraefikService")

  set_found_crd=$(
  for crd in ${crd_apis[@]}; do
    echo "# {{- set \$found \"${crd}\" false -}}"
  done
  )

  # Copy base template and apply variables to the template
  mkdir -p ${TRAEFIK_TMP_CRD}
  echo $(pwd)
  cp -R ./k3s-1.24.2-rc1-k3s2/scripts/chart-templates/crd-base/* ${TRAEFIK_TMP_CRD}
  for template_file in $(find ${TRAEFIK_TMP_CRD} -type f | sort); do
    # Applies any environment variables currently set onto your template file
    echo "Templating ${template_file}"
    eval "echo \"$(sed 's/"/\\"/g' ${template_file})\"" > ${template_file}
  done

  # Move anything from ${f}/charts-crd/overlay-upstream to the main chart
  cp -R ${TRAEFIK_TMP_CRD}/overlay-upstream/* ${TRAEFIK_TMP_CHART}
  rm -rf ${TRAEFIK_TMP_CRD}/overlay-upstream

  # Modify charts to support system-default-registry
  echo -e 'global:\n  systemDefaultRegistry: ""' >> ${TRAEFIK_TMP_CHART}/values.yaml
  find ${TRAEFIK_TMP_CHART} -type f | xargs sed -i 's/{{ .Values.image.name }}/{{ template "system_default_registry" .}}&/g'

  # Modify chart version to append package version
  # If we alter our repackaging of the helm chart without also bumping the version of the
  # chart, the package version portion (final two digits) of the version string in the
  # traefik HelmChart manifest should be bumped accordingly.
  sed -i "s/version: .*/&${TRAEFIK_PACKAGE_VERSION}/" ${TRAEFIK_TMP_CHART}/Chart.yaml

  # Add dashboard annotations to main chart
  cat <<EOF >>${TRAEFIK_TMP_CHART}/Chart.yaml
annotations:
  fleet.cattle.io/bundle-id: k3s
EOF

  # Move CRDs from main chart to CRD chart
  mkdir -p ${TRAEFIK_TMP_CRD}/templates
  mv ${TRAEFIK_TMP_CHART}/crds/* ${TRAEFIK_TMP_CRD}/templates
  rm -rf ${TRAEFIK_TMP_CHART}/crds

  # Package charts
  OPTS="--format=gnu --sort=name --owner=0 --group=0 --mode=gou-s --numeric-owner --no-acls --no-selinux --no-xattrs"
  tar ${OPTS} --mtime='2021-01-01 00:00:00Z' -cf - -C ${TMP_DIR} $(basename ${TRAEFIK_TMP_CHART}) | gzip -n > ${CHARTS_DIR}/${TRAEFIK_FILE}
  tar ${OPTS} --mtime='2021-01-01 00:00:00Z' -cf - -C ${TMP_DIR} $(basename ${TRAEFIK_TMP_CRD}) | gzip -n > ${CHARTS_DIR}/${TRAEFIK_CRD_FILE}
  for TAR in ${CHARTS_DIR}/${TRAEFIK_FILE} ${CHARTS_DIR}/${TRAEFIK_CRD_FILE}; do
    sha256sum ${TAR}
    stat ${TAR}
    tar -vtf ${TAR}
  done
}

download_and_package_traefik
tar -cvf build.tar.gz ./build
rm -rf ./build
rm -rf traefik*
rm -rf ./k3s-1.24.2-rc1-k3s2

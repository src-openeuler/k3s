%define debug_package %{nil}
%define baseversion 1.24

Name:           k3s
Version:        1.24.2+rc1+k3s2
Release:        4
Summary:        Installer for k3s optimised for openSUSE MicroOS
License:        Apache-2.0
Group:          System/Management
URL:            https://k3s.io
Source0:        https://github.com/k3s-io/k3s/archive/v%{version}.tar.gz#/v%{version}.tar.gz
Source1:	build.tar.gz
Source2:        build-tar-create.sh

BuildRequires:  golang
BuildRequires:  libseccomp-devel
BuildRequires:  btrfs-progs-devel
BuildRequires:  k3s-plugins
BuildRequires:  k3s-containerd
BuildRequires:  runc 
BuildRequires:  systemd systemd-devel
Requires:       container-selinux
Requires:       k3s-selinux
Requires:       iptables
Conflicts:      cri-tools
Conflicts:      kubectl
Conflicts:      kubernetes-client
Conflicts:      kubernetes-client-provider

Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd


%description
Based on the official upstream k3s install.sh, this k3s-install
package provides a curated, MicroOS-optimised, securely delivered
alternative to running a script directly from the internet.

k3s is a container orchestration system for automating application
deployment, scaling, and management. It is a Kubernetes-compliant
distribution that differs from the original Kubernetes (colloquially
"k8s") in that:

  * Legacy, alpha, or non-default features are removed.
  * Most in-tree plugins (cloud providers and storage plugins) were
    removed, since they can be replaced with out-of-tree addons.
  * sqlite3 is the default storage mechanism.
    etcd3 is still available, but not the default.
  * There is a new launcher that handles a lot of the complexity of
    TLS and options.

%prep
%autosetup -p1 -n k3s-%(echo %{version} | tr '+' '-')
cp %{SOURCE1} .
sed -i '/echo Building runc/,+5d' scripts/build
cp /usr/share/k3s/cni bin/

%build
export GOPATH=%{_builddir}/go
export PATH=$PATH:${GOPATH}/bin
export GO111MODULE=on
export GOPROXY=https://repo.huaweicloud.com/repository/goproxy/
export GONOSUMDB=*

tar -zxvf build.tar.gz
pushd build
mkdir -p ./src/github.com/containerd/containerd
cp -rf /usr/libexec/containerd/containerd-1.6.6/. src/github.com/containerd/containerd/
popd

mkdir etc 
./scripts/build

cp /usr/bin/runc bin/

SKIP_AIRGAP=true SKIP_IMAGE=true ./scripts/package

%install
install -D -m 755 dist/artifacts/k3s* %{buildroot}/usr/local/bin/k3s
mkdir -p %{buildroot}%{_localstatedir}/lib/rancher/k3s
mkdir -p %{buildroot}%{_localstatedir}/lib/rancher/k3s/server/manifests
cp -rf manifests/* %{buildroot}%{_localstatedir}/lib/rancher/k3s/server/manifests
mkdir -p %{buildroot}%{_sbindir}
cp install.sh %{buildroot}%{_sbindir}/k3s-install.sh


%files
%license LICENSE
%doc README.md
/usr/local/bin/k3s
%{_localstatedir}/lib/rancher
%{_sbindir}/k3s-install.sh

%changelog
* Sun Apr 21 2024 Yuncheng Zhu <zhuyuncheng@huawei.com> - 1.24.2+rc1+k3s2-4
- change requirement docker-runc to runc

* Feb Nov 01 2022 Ge Wang <wangge20@h-partners.com> - 1.24.2+rc1+k3s2-3
- add flannel plugin to project to fix sandbox creating failure

* Wed Aug 31 2022 Yuncheng Zhu <zhuyuncheng@huawei.com> - 1.24.2+rc1+k3s2-2
- add missed requires

* Tue Jul 26 2022 Ge Wang <wangge20@h-partners.com> - 1.24.2+rc1+k3s2-1
- Init package
